import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:skllzz_editor/common/pair_triplet.dart';
import 'package:skllzz_editor/generated/models/models.pb.dart' as gena;
import 'package:skllzz_editor/generated/models/models.pb.dart';
import 'package:skllzz_editor/providers/cache.dart';

import '../../theming.dart';

// final actionProvider =
//     StateProvider.family<gena.Action, String>((ref, id) => gena.Action(id: id));

enum BlockType { message, quiz }

Map<BlockType, String> typeNames = {
  BlockType.message: "Сообщение",
  BlockType.quiz: "Квиз",
};

Map<BlockType, Color> typeColors = {
  BlockType.message: Styles.backgroundCard,
  BlockType.quiz: Styles.blueAccentColor,
};

class PostBlock extends StatefulWidget {
  final gena.Action action;
  final double elevation;

  const PostBlock({Key? key, this.elevation = 3, required this.action})
      : super(key: key);

  @override
  _PostBlockState createState() => _PostBlockState();
}

class _PostBlockState extends State<PostBlock>
    with AutomaticKeepAliveClientMixin {
  late TextEditingController _blockNameController;

  late StateProvider typeProvider;
  BlockType initial = BlockType.message;

  @override
  void initState() {
    super.initState();

    if (widget.action.hasQuestion()) {
      initial = BlockType.quiz;
    }
    typeProvider = StateProvider<BlockType>((ref) => initial);
    _blockNameController = TextEditingController(text: typeNames[initial]);
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Consumer(builder: (context, watch, _) {
      // final action = watch(actionProvider(widget.action.state.id));
      final type = watch(typeProvider);

      if (type.state == BlockType.message) {
        if (!widget.action.hasMessage()) {
          widget.action.clearQuestion();
          widget.action.message = Message();
        }
      } else {
        if (!widget.action.hasQuestion()) {
          widget.action.clearMessage();
          widget.action.question = Question();
        }
      }

      if (_blockNameController.text == typeNames[initial]) {
        _blockNameController.text = typeNames[type.state]!;
      }

      log(widget.action.toDebugString());

      return Card(
          color: typeColors[type.state],
          elevation: widget.elevation,
          shadowColor: typeColors[type.state],
          child: Padding(
              padding: const EdgeInsets.all(15.0),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: 200,
                      child: TextField(
                          controller: _blockNameController,
                          style: Theme.of(context).textTheme.headline6,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      width: 1, color: Styles.actionColor)))),
                    ),
                    Container(height: 15),
                    Card(
                      elevation: 0,
                      color: Styles.actionColor.withOpacity(0.15),
                      child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 5),
                        child: DropdownButton<BlockType>(
                            value: type.state,
                            underline: Container(),
                            icon: Icon(Icons.arrow_downward_rounded,
                                color: Styles.actionColor),
                            onChanged: (BlockType? newValue) {
                              type.state = newValue!;
                            },
                            selectedItemBuilder: (context) {
                              return BlockType.values
                                  .map<DropdownMenuItem<BlockType>>(
                                      (BlockType value) {
                                return DropdownMenuItem<BlockType>(
                                  value: value,
                                  child: Text.rich(TextSpan(children: [
                                    TextSpan(
                                        text: "Тип поста: ",
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText1),
                                    TextSpan(
                                        text: typeNames[value],
                                        style: Theme.of(context)
                                            .textTheme
                                            .subtitle1)
                                  ])),
                                );
                              }).toList();
                            },
                            items: BlockType.values
                                .map<DropdownMenuItem<BlockType>>(
                                    (BlockType value) {
                              return DropdownMenuItem<BlockType>(
                                value: value,
                                child: Text(typeNames[value]!),
                              );
                            }).toList()),
                      ),
                    ),
                    Container(height: 15),
                    if (type.state == BlockType.quiz)
                      QuestionBlock(action: widget.action)
                    else
                      MessageBlock(action: widget.action),
                  ])));
    });
  }

  @override
  bool get wantKeepAlive => true;
}

class MessageBlock extends StatefulWidget {
  final gena.Action action;

  const MessageBlock({Key? key, required this.action}) : super(key: key);

  @override
  _MessageBlockState createState() => _MessageBlockState();
}

class _MessageBlockState extends State<MessageBlock> {
  late TextEditingController messageTextController;

  @override
  void initState() {
    super.initState();
    messageTextController =
        TextEditingController(text: widget.action.message.text);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            width: 300,
            clipBehavior: Clip.antiAlias,
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(15)),
            child: TextField(
                controller: messageTextController,
                minLines: 4,
                maxLines: 10,
                style: Theme.of(context).textTheme.bodyText1,
                onChanged: (text) {
                  widget.action.message.text = text;
                  setState(() {});
                },
                decoration: InputDecoration(
                    filled: true,
                    fillColor: Styles.actionColor.withOpacity(0.15),
                    border: InputBorder.none,
                    labelText: "Текст сообщения",
                    hintText: "Введите текст сообщения")),
          )
        ]);
  }
}

class QuestionBlock extends StatefulWidget {
  final gena.Action action;

  const QuestionBlock({Key? key, required this.action}) : super(key: key);

  @override
  _QuestionBlockState createState() => _QuestionBlockState();
}

class _QuestionBlockState extends State<QuestionBlock> {
  late TextEditingController questionTextController;

  late StateProvider<Map<String, Pair<TextEditingController>>>
      answerListController;

  late StateProvider<gena.Question> questionProvider;

  @override
  void initState() {
    super.initState();
    questionTextController =
        TextEditingController(text: widget.action.question.text);
    answerListController =
        StateProvider<Map<String, Pair<TextEditingController>>>((ref) {
      final res = Map<String, Pair<TextEditingController>>();
      for (final a in widget.action.question.answers) {
        res[a.id] = Pair(TextEditingController(text: a.text),
            TextEditingController(text: a.award.toString()));
      }
      return res;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Consumer(builder: (context, watch, _) {
      final answersControllers = watch(answerListController);
      // final question = watch(questionProvider);
      // final action = watch(actionProvider(widget.id));

      return Container(
          width: 400,
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  clipBehavior: Clip.antiAlias,
                  decoration:
                      BoxDecoration(borderRadius: BorderRadius.circular(15)),
                  child: TextField(
                      controller: questionTextController,
                      minLines: 1,
                      maxLines: 3,
                      style: Theme.of(context).textTheme.bodyText1,
                      onChanged: (text) {
                        widget.action.question.text = text;
                        setState(() {});
                      },
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: Styles.actionColor.withOpacity(0.15),
                          border: InputBorder.none,
                          labelText: "Вопрос",
                          hintText: "Введите текст вопроса")),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 5.0),
                  child: Row(children: [
                    Text("Ответы (до 8):"),
                    Spacer(),
                    FloatingActionButton(
                      onPressed: () {
                        if (answersControllers.state.length < 8) {
                          final aid = Cache.uuid.v4();
                          answersControllers.state = answersControllers.state
                            ..[aid] = Pair(TextEditingController(),
                                TextEditingController());
                          widget.action.question.answers
                              .add(gena.Answer(id: aid));
                          setState(() {});
                        }
                      },
                      child: Icon(
                        Icons.add_rounded,
                        color: typeColors[BlockType.quiz],
                        size: 32,
                      ),
                      backgroundColor: Styles.actionColor,
                      mini: true,
                    )
                  ]),
                ),
                Container(
                  child: ListView.builder(
                      shrinkWrap: true,
                      padding: EdgeInsets.zero,
                      itemCount: widget.action.question.answers.length,
                      itemBuilder: (context, index) {
                        final a = widget.action.question.answers[index];
                        return Padding(
                          padding: const EdgeInsets.only(bottom: 8.0),
                          child: Row(children: [
                            Expanded(
                              child: Container(
                                  clipBehavior: Clip.antiAlias,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(15)),
                                  child: TextField(
                                      controller:
                                          answersControllers.state[a.id]?.a,
                                      minLines: 1,
                                      maxLines: 1,
                                      style:
                                          Theme.of(context).textTheme.bodyText1,
                                      onChanged: (text) {
                                        a.text = text;
                                        setState(() {});
                                      },
                                      decoration: InputDecoration(
                                          filled: true,
                                          fillColor: Styles.actionColor
                                              .withOpacity(0.15),
                                          border: InputBorder.none,
                                          labelText: "Ответ",
                                          hintText: "Введите ответ"))),
                            ),
                            Container(width: 5),
                            Container(
                                width: 100,
                                clipBehavior: Clip.antiAlias,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15)),
                                child: TextField(
                                    controller:
                                        answersControllers.state[a.id]?.b,
                                    minLines: 1,
                                    maxLines: 3,
                                    style:
                                        Theme.of(context).textTheme.bodyText1,
                                    onChanged: (text) {
                                      a.award = double.tryParse(text) ?? 0;
                                      setState(() {});
                                    },
                                    decoration: InputDecoration(
                                        filled: true,
                                        fillColor: Styles.actionColor
                                            .withOpacity(0.15),
                                        border: InputBorder.none,
                                        labelText: "Вес",
                                        hintText: "5.0"))),
                            IconButton(
                                onPressed: () {
                                  answersControllers.state =
                                      answersControllers.state..remove(a.id);
                                  widget.action.question.answers
                                      .removeAt(index);
                                  setState(() {});
                                },
                                icon: Icon(Icons.remove_circle_outline_rounded))
                          ]),
                        );
                      }),
                ),
              ]));
    });
  }
}

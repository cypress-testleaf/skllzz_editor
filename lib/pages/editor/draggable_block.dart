import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:skllzz_editor/theming.dart';
import 'package:widget_arrows/widget_arrows.dart';
import 'package:skllzz_editor/generated/models/models.pb.dart' as gena;

import '../../main.dart';
import 'blocks.dart';

typedef ElevationBuilder = Widget Function(BuildContext context, double elevation);

Map<String, String> arrows = {};

final showPointProvider = StateProvider<bool>((ref) => false);

String currentId = "";

class DraggableBlock extends StatefulWidget {
  final gena.Action action;
  final String? targetId;
  final Offset? initialPosition;
  final ElevationBuilder builder;

  const DraggableBlock({Key? key, required this.builder, this.initialPosition, required this.action, this.targetId})
      : super(key: key);

  @override
  _DraggableBlockState createState() => _DraggableBlockState();
}

class _DraggableBlockState extends State<DraggableBlock> {
  final showArrowProvider = StateProvider<bool>((ref) => false);

  late StateProvider<Offset> positionProvider;

  @override
  void initState() {
    super.initState();
    positionProvider =
        StateProvider<Offset>((ref) => widget.initialPosition ?? Offset(0, 0));
  }

  double checkConstraints(double x, double max) {
    return x < 0 ? 0 : (x > max ? max : x);
  }

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (context, watch, _) {
        final showArrow = watch(showArrowProvider);
        final showPoint = watch(showPointProvider);

        final positionState = watch(positionProvider);
        final position = watch(positionProvider).state;

        return Positioned(
          top: position.dy,
          left: position.dx,
          child: Stack(
              clipBehavior: Clip.none,
              alignment: Alignment.centerRight,
              children: [
                Draggable(
                  childWhenDragging: Container(
                      foregroundDecoration: BoxDecoration(
                          color: Styles.backgroundColor.withOpacity(0.9)),
                      child: widget.builder(context, 0)), //widget.child),
                  feedback: widget.builder(context, 20),
                  onDragEnd: (details) {
                    positionState.state = Offset(
                        checkConstraints(details.offset.dx,
                            MediaQuery.of(context).size.width),
                        checkConstraints(details.offset.dy,
                            MediaQuery.of(context).size.height));
                  },
                  child: ArrowElement(
                      id: widget.action.id,
                      targetId: arrows[widget.action.id] ?? null,
                      sourceAnchor: Alignment.centerRight,
                      color: Styles.actionColor,
                      child: widget.builder(context, 3)),
                ),
                Positioned(
                    right: -20,
                    child: MouseRegion(
                      onEnter: (event) {
                        showArrow.state = true;
                      },
                      onExit: (event) {
                        showArrow.state = false;
                      },
                      child: IconButton(
                        splashRadius: 24,
                        visualDensity: VisualDensity.compact,
                        icon: Icon(Icons.arrow_forward_rounded),
                        color: Styles.actionColor
                            .withOpacity(showArrow.state ? 1 : 0),
                        onPressed: () {
                          showPoint.state = true;
                          currentId = widget.action.id;
                        },
                      ),
                    )),
                Positioned(
                    left: -24,
                    child: IconButton(
                        splashRadius: 24,
                        visualDensity: VisualDensity.compact,
                        icon: Container(
                          width: 30,
                          height: 30,
                          decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Styles.brightGreenColor.withOpacity((showPoint.state) && (currentId != widget.action.id) ? 0.5 : 0)
                        ),),
                        color: Styles.actionColor
                            .withOpacity(showArrow.state ? 1 : 0),
                        onPressed: () {
                          setState(() {
                            showPoint.state = false;
                            arrows.addAll({currentId: widget.action.id});
                            currentId = "";
                          });
                        },
                    )),
              ]),
        );
      },
    );
  }
}

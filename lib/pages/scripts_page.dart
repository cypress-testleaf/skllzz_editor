import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:intl/intl.dart';
import 'package:skllzz_editor/generated/grpc/skllzz/management/script.pb.dart';
import 'package:skllzz_editor/providers/info_provider.dart';
import 'package:skllzz_editor/providers/scripts_provider.dart';
import 'package:skllzz_editor/widgets/adaptive/adaptive_insets.dart';
import 'package:skllzz_editor/widgets/custom/custom_table.dart';
import 'package:skllzz_editor/widgets/simple_search_panel.dart';
import 'package:skllzz_editor/widgets/top_app_bar.dart';
import 'package:skllzz_editor/widgets/wrappers/error.dart';

import '../formatters.dart';
import '../theming.dart';

class ScriptsPage extends ConsumerWidget {
  @override
  Widget build(BuildContext context, ScopedReader watch) {
    final search = watch(scriptSearchProvider);

    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Scaffold(
        backgroundColor: Styles.backgroundColor,
        extendBodyBehindAppBar: false,
        appBar: TopAppBar(
          label: "База скриптов",
          hasBlur: false,
          hasBackArrow: false,
        ),
        body: Center(
            child: Container(
          constraints: BoxConstraints(maxWidth: 900, minWidth: 600),
          child: Column(mainAxisSize: MainAxisSize.min, children: [
            Container(
                padding: AdaptiveInsets.only(
                    MediaQuery.of(context).size.width < 700,
                    right: 10),
                child: Card(
                    color: Styles.backgroundInfo,
                    elevation: 0,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(50.0))),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 3.0),
                      child: Theme(
                          data: Styles.lightTheme,
                          child: Row(children: [
                            Expanded(
                              child: SimpleSearchPanel(
                                inputDecoration: InputDecoration(
                                    hintText: "Найти скрипты",
                                    border: InputBorder.none,
                                    labelText: "Поиск"),
                                onPressedAction: (query) {
                                  search.state = query;
                                },
                                onCancel: () {
                                  search.state = "";
                                },
                                label: "искать",
                              ),
                            )
                          ])),
                    ))),
            Expanded(
                child: (MediaQuery.of(context).size.width > 700)
                    ? Container(
                        constraints: BoxConstraints(maxWidth: 1200),
                        padding: EdgeInsets.only(top: 0, right: 10),
                        child: ScriptTableList())
                    : SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Container(
                            constraints: BoxConstraints(maxWidth: 700),
                            child: ScriptTableList()))),
          ]),
        )),
      ),
    );
  }
}

class ScriptTableList extends ConsumerWidget {
  @override
  Widget build(BuildContext context, watch) {
    final scriptClient = watch(scriptListProvider);

    return scriptClient.when(
        data: (scripts) {
          return TableBuilder<Script>(
              list: scripts,
              headerContent: [Text("Имя"), Container(), Text("Описание")],
              rowContent: (context, script) {
                final createdDate = DateTime.fromMillisecondsSinceEpoch(
                    script.createdMillis.toInt());
                final modifiedDate = DateTime.fromMillisecondsSinceEpoch(
                    script.modifiedMillis.toInt());
                return [
                  SelectableText(script.name,
                      style: Theme.of(context).textTheme.subtitle2),
                  Column(
                    children: [
                      Text(
                        textDate(createdDate) +
                            " " +
                            DateFormat.jm().format(createdDate),
                        style: Theme.of(context).textTheme.bodyText2,
                      ),
                      Text(
                          textDate(modifiedDate) +
                              " " +
                              DateFormat.jm().format(modifiedDate),
                          style: Theme.of(context).textTheme.bodyText2),
                    ],
                  ),
                  Text(
                    script.description,
                    style: Theme.of(context).textTheme.bodyText2,
                  ),
                ];
              },
              widthWrappers: [
                (context, child) => Expanded(
                    child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 8),
                        child: child)),
                (context, child) => Container(
                      padding: EdgeInsets.symmetric(horizontal: 8),
                      width: 150 * MediaQuery.of(context).textScaleFactor,
                      child: child,
                    ),
                (context, child) => Expanded(
                    flex: 2,
                    child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 8),
                        child: child)),
              ]);
        },
        loading: () => Center(
              child: Container(
                  width: 100, height: 100, child: Styles.loadingAnimation),
            ),
        error: (e, s) {
          context.read(errorProvider).state =
              ExceptionCause(e, "Не удалось загрузить скрипты");
          return LoadingErrorMessage(message: "Не удалось загрузить скрипты");
        });
  }
}

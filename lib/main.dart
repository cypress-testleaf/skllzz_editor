import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:skllzz_editor/pages/editor/main_editor.dart';
import 'package:skllzz_editor/theming.dart';

import 'gena.dart';
import 'skllzz_app_icons.dart';
import 'theming.dart';

void main() {
  runApp(ProviderScope(child: SkllzzEditor()));
}

class SkllzzEditor extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: Styles.mainTheme,
      home: HomePage(title: 'SKLLZZ Editor'),
    );
  }
}

class HomePage extends StatefulWidget {
  HomePage({Key? key, this.title}) : super(key: key);
  final String? title;

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Scaffold(
          body: MainEditor()),
    );
  }
}

/// Плагины
import 'package:uuid/uuid.dart';
import 'package:uuid/uuid_util.dart';

class Cache {
  static final instance = Cache._();
  static bool forceRefreshToken = false;
  var googleIdToken = "";
  static final uuid = Uuid(options: {'grng': UuidUtil.cryptoRNG});

  bool autoLogin = false;

  Cache._();
}

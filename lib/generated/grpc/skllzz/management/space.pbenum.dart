///
//  Generated code. Do not modify.
//  source: skllzz/management/space.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

// ignore_for_file: UNDEFINED_SHOWN_NAME
import 'dart:core' as $core;
import 'package:protobuf/protobuf.dart' as $pb;

class SpaceRole extends $pb.ProtobufEnum {
  static const SpaceRole ACCESS = SpaceRole._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'ACCESS');
  static const SpaceRole CLIENT_VIEW = SpaceRole._(1, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'CLIENT_VIEW');
  static const SpaceRole CLIENT_EDIT = SpaceRole._(2, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'CLIENT_EDIT');
  static const SpaceRole MANAGER_VIEW = SpaceRole._(3, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'MANAGER_VIEW');
  static const SpaceRole MANAGER_ROLES = SpaceRole._(4, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'MANAGER_ROLES');
  static const SpaceRole SPACE_MANAGE = SpaceRole._(5, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'SPACE_MANAGE');
  static const SpaceRole SPACE_INVITE = SpaceRole._(6, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'SPACE_INVITE');
  static const SpaceRole POS_VIEW = SpaceRole._(7, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'POS_VIEW');
  static const SpaceRole POS_MANAGE = SpaceRole._(8, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'POS_MANAGE');
  static const SpaceRole TRANSACTIONS_VIEW = SpaceRole._(9, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'TRANSACTIONS_VIEW');
  static const SpaceRole TRANSACTIONS_APPLY = SpaceRole._(10, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'TRANSACTIONS_APPLY');
  static const SpaceRole SCRIPT_VIEW = SpaceRole._(11, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'SCRIPT_VIEW');
  static const SpaceRole SCRIPT_READ = SpaceRole._(12, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'SCRIPT_READ');
  static const SpaceRole SCRIPT_EDIT = SpaceRole._(13, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'SCRIPT_EDIT');
  static const SpaceRole CLIENT_IMPERSONATE = SpaceRole._(14, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'CLIENT_IMPERSONATE');

  static const $core.List<SpaceRole> values = <SpaceRole> [
    ACCESS,
    CLIENT_VIEW,
    CLIENT_EDIT,
    MANAGER_VIEW,
    MANAGER_ROLES,
    SPACE_MANAGE,
    SPACE_INVITE,
    POS_VIEW,
    POS_MANAGE,
    TRANSACTIONS_VIEW,
    TRANSACTIONS_APPLY,
    SCRIPT_VIEW,
    SCRIPT_READ,
    SCRIPT_EDIT,
    CLIENT_IMPERSONATE,
  ];

  static final $core.Map<$core.int, SpaceRole> _byValue = $pb.ProtobufEnum.initByValue(values);
  static SpaceRole? valueOf($core.int value) => _byValue[value];

  const SpaceRole._($core.int v, $core.String n) : super(v, n);
}


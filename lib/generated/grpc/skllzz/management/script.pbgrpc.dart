///
//  Generated code. Do not modify.
//  source: skllzz/management/script.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'search.pb.dart' as $1;
import 'script.pb.dart' as $2;
export 'script.pb.dart';

class ScriptManagementClient extends $grpc.Client {
  static final _$monitorScripts = $grpc.ClientMethod<$1.QueryFilter, $2.Script>(
      '/com.skllzz.api.ScriptManagement/MonitorScripts',
      ($1.QueryFilter value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $2.Script.fromBuffer(value));
  static final _$deleteScript = $grpc.ClientMethod<$2.Script, $2.Script>(
      '/com.skllzz.api.ScriptManagement/DeleteScript',
      ($2.Script value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $2.Script.fromBuffer(value));
  static final _$updateScript = $grpc.ClientMethod<$2.Script, $2.Script>(
      '/com.skllzz.api.ScriptManagement/UpdateScript',
      ($2.Script value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $2.Script.fromBuffer(value));
  static final _$writeScript = $grpc.ClientMethod<$2.DataBlock, $2.Script>(
      '/com.skllzz.api.ScriptManagement/WriteScript',
      ($2.DataBlock value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $2.Script.fromBuffer(value));
  static final _$readScript = $grpc.ClientMethod<$2.Script, $2.DataBlock>(
      '/com.skllzz.api.ScriptManagement/ReadScript',
      ($2.Script value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $2.DataBlock.fromBuffer(value));

  ScriptManagementClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseStream<$2.Script> monitorScripts($1.QueryFilter request,
      {$grpc.CallOptions? options}) {
    return $createStreamingCall(
        _$monitorScripts, $async.Stream.fromIterable([request]),
        options: options);
  }

  $grpc.ResponseFuture<$2.Script> deleteScript($2.Script request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$deleteScript, request, options: options);
  }

  $grpc.ResponseFuture<$2.Script> updateScript($2.Script request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$updateScript, request, options: options);
  }

  $grpc.ResponseFuture<$2.Script> writeScript(
      $async.Stream<$2.DataBlock> request,
      {$grpc.CallOptions? options}) {
    return $createStreamingCall(_$writeScript, request, options: options)
        .single;
  }

  $grpc.ResponseStream<$2.DataBlock> readScript($2.Script request,
      {$grpc.CallOptions? options}) {
    return $createStreamingCall(
        _$readScript, $async.Stream.fromIterable([request]),
        options: options);
  }
}

abstract class ScriptManagementServiceBase extends $grpc.Service {
  $core.String get $name => 'com.skllzz.api.ScriptManagement';

  ScriptManagementServiceBase() {
    $addMethod($grpc.ServiceMethod<$1.QueryFilter, $2.Script>(
        'MonitorScripts',
        monitorScripts_Pre,
        false,
        true,
        ($core.List<$core.int> value) => $1.QueryFilter.fromBuffer(value),
        ($2.Script value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$2.Script, $2.Script>(
        'DeleteScript',
        deleteScript_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $2.Script.fromBuffer(value),
        ($2.Script value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$2.Script, $2.Script>(
        'UpdateScript',
        updateScript_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $2.Script.fromBuffer(value),
        ($2.Script value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$2.DataBlock, $2.Script>(
        'WriteScript',
        writeScript,
        true,
        false,
        ($core.List<$core.int> value) => $2.DataBlock.fromBuffer(value),
        ($2.Script value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$2.Script, $2.DataBlock>(
        'ReadScript',
        readScript_Pre,
        false,
        true,
        ($core.List<$core.int> value) => $2.Script.fromBuffer(value),
        ($2.DataBlock value) => value.writeToBuffer()));
  }

  $async.Stream<$2.Script> monitorScripts_Pre(
      $grpc.ServiceCall call, $async.Future<$1.QueryFilter> request) async* {
    yield* monitorScripts(call, await request);
  }

  $async.Future<$2.Script> deleteScript_Pre(
      $grpc.ServiceCall call, $async.Future<$2.Script> request) async {
    return deleteScript(call, await request);
  }

  $async.Future<$2.Script> updateScript_Pre(
      $grpc.ServiceCall call, $async.Future<$2.Script> request) async {
    return updateScript(call, await request);
  }

  $async.Stream<$2.DataBlock> readScript_Pre(
      $grpc.ServiceCall call, $async.Future<$2.Script> request) async* {
    yield* readScript(call, await request);
  }

  $async.Stream<$2.Script> monitorScripts(
      $grpc.ServiceCall call, $1.QueryFilter request);
  $async.Future<$2.Script> deleteScript(
      $grpc.ServiceCall call, $2.Script request);
  $async.Future<$2.Script> updateScript(
      $grpc.ServiceCall call, $2.Script request);
  $async.Future<$2.Script> writeScript(
      $grpc.ServiceCall call, $async.Stream<$2.DataBlock> request);
  $async.Stream<$2.DataBlock> readScript(
      $grpc.ServiceCall call, $2.Script request);
}

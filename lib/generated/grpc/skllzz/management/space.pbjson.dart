///
//  Generated code. Do not modify.
//  source: skllzz/management/space.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use spaceRoleDescriptor instead')
const SpaceRole$json = const {
  '1': 'SpaceRole',
  '2': const [
    const {'1': 'ACCESS', '2': 0},
    const {'1': 'CLIENT_VIEW', '2': 1},
    const {'1': 'CLIENT_EDIT', '2': 2},
    const {'1': 'MANAGER_VIEW', '2': 3},
    const {'1': 'MANAGER_ROLES', '2': 4},
    const {'1': 'SPACE_MANAGE', '2': 5},
    const {'1': 'SPACE_INVITE', '2': 6},
    const {'1': 'POS_VIEW', '2': 7},
    const {'1': 'POS_MANAGE', '2': 8},
    const {'1': 'TRANSACTIONS_VIEW', '2': 9},
    const {'1': 'TRANSACTIONS_APPLY', '2': 10},
    const {'1': 'SCRIPT_VIEW', '2': 11},
    const {'1': 'SCRIPT_READ', '2': 12},
    const {'1': 'SCRIPT_EDIT', '2': 13},
    const {'1': 'CLIENT_IMPERSONATE', '2': 14},
  ],
};

/// Descriptor for `SpaceRole`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List spaceRoleDescriptor = $convert.base64Decode('CglTcGFjZVJvbGUSCgoGQUNDRVNTEAASDwoLQ0xJRU5UX1ZJRVcQARIPCgtDTElFTlRfRURJVBACEhAKDE1BTkFHRVJfVklFVxADEhEKDU1BTkFHRVJfUk9MRVMQBBIQCgxTUEFDRV9NQU5BR0UQBRIQCgxTUEFDRV9JTlZJVEUQBhIMCghQT1NfVklFVxAHEg4KClBPU19NQU5BR0UQCBIVChFUUkFOU0FDVElPTlNfVklFVxAJEhYKElRSQU5TQUNUSU9OU19BUFBMWRAKEg8KC1NDUklQVF9WSUVXEAsSDwoLU0NSSVBUX1JFQUQQDBIPCgtTQ1JJUFRfRURJVBANEhYKEkNMSUVOVF9JTVBFUlNPTkFURRAO');
@$core.Deprecated('Use spaceDescriptor instead')
const Space$json = const {
  '1': 'Space',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '8': const {}, '10': 'id'},
    const {'1': 'name', '3': 2, '4': 1, '5': 9, '8': const {}, '10': 'name'},
    const {'1': 'deleted', '3': 9999, '4': 1, '5': 8, '10': 'deleted'},
  ],
};

/// Descriptor for `Space`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List spaceDescriptor = $convert.base64Decode('CgVTcGFjZRIUCgJpZBgBIAEoCUIEiLUYAVICaWQSGAoEbmFtZRgCIAEoCUIEiLUYAVIEbmFtZRIZCgdkZWxldGVkGI9OIAEoCFIHZGVsZXRlZA==');
@$core.Deprecated('Use managerDescriptor instead')
const Manager$json = const {
  '1': 'Manager',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '8': const {}, '10': 'id'},
    const {'1': 'name', '3': 2, '4': 1, '5': 9, '8': const {}, '10': 'name'},
    const {'1': 'role', '3': 3, '4': 3, '5': 14, '6': '.com.skllzz.api.SpaceRole', '8': const {}, '10': 'role'},
    const {'1': 'template', '3': 4, '4': 1, '5': 8, '10': 'template'},
    const {'1': 'deleted', '3': 9999, '4': 1, '5': 8, '10': 'deleted'},
  ],
};

/// Descriptor for `Manager`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List managerDescriptor = $convert.base64Decode('CgdNYW5hZ2VyEhQKAmlkGAEgASgJQgSItRgBUgJpZBIYCgRuYW1lGAIgASgJQgSItRgBUgRuYW1lEjMKBHJvbGUYAyADKA4yGS5jb20uc2tsbHp6LmFwaS5TcGFjZVJvbGVCBIi1GAFSBHJvbGUSGgoIdGVtcGxhdGUYBCABKAhSCHRlbXBsYXRlEhkKB2RlbGV0ZWQYj04gASgIUgdkZWxldGVk');
@$core.Deprecated('Use spaceAuthorizationDescriptor instead')
const SpaceAuthorization$json = const {
  '1': 'SpaceAuthorization',
  '2': const [
    const {'1': 'token', '3': 1, '4': 1, '5': 9, '10': 'token'},
    const {'1': 'relogin', '3': 2, '4': 1, '5': 8, '10': 'relogin'},
    const {'1': 'roles', '3': 3, '4': 3, '5': 14, '6': '.com.skllzz.api.SpaceRole', '10': 'roles'},
    const {'1': 'space_id', '3': 4, '4': 1, '5': 9, '10': 'spaceId'},
  ],
};

/// Descriptor for `SpaceAuthorization`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List spaceAuthorizationDescriptor = $convert.base64Decode('ChJTcGFjZUF1dGhvcml6YXRpb24SFAoFdG9rZW4YASABKAlSBXRva2VuEhgKB3JlbG9naW4YAiABKAhSB3JlbG9naW4SLwoFcm9sZXMYAyADKA4yGS5jb20uc2tsbHp6LmFwaS5TcGFjZVJvbGVSBXJvbGVzEhkKCHNwYWNlX2lkGAQgASgJUgdzcGFjZUlk');
@$core.Deprecated('Use inviteTicketDescriptor instead')
const InviteTicket$json = const {
  '1': 'InviteTicket',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'space_id', '3': 2, '4': 1, '5': 9, '10': 'spaceId'},
    const {'1': 'template', '3': 3, '4': 1, '5': 11, '6': '.com.skllzz.api.Manager', '10': 'template'},
  ],
};

/// Descriptor for `InviteTicket`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List inviteTicketDescriptor = $convert.base64Decode('CgxJbnZpdGVUaWNrZXQSDgoCaWQYASABKAlSAmlkEhkKCHNwYWNlX2lkGAIgASgJUgdzcGFjZUlkEjMKCHRlbXBsYXRlGAMgASgLMhcuY29tLnNrbGx6ei5hcGkuTWFuYWdlclIIdGVtcGxhdGU=');

///
//  Generated code. Do not modify.
//  source: skllzz/common/common.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

// ignore_for_file: UNDEFINED_SHOWN_NAME
import 'dart:core' as $core;
import 'package:protobuf/protobuf.dart' as $pb;

class PosCategory extends $pb.ProtobufEnum {
  static const PosCategory other = PosCategory._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'other');
  static const PosCategory fitness = PosCategory._(1, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'fitness');
  static const PosCategory food = PosCategory._(3, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'food');
  static const PosCategory beauty = PosCategory._(7, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'beauty');
  static const PosCategory clothes = PosCategory._(5, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'clothes');
  static const PosCategory hardware = PosCategory._(10, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'hardware');
  static const PosCategory grocery = PosCategory._(4, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'grocery');
  static const PosCategory medicine = PosCategory._(2, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'medicine');
  static const PosCategory entertainment = PosCategory._(6, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'entertainment');
  static const PosCategory travel = PosCategory._(8, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'travel');
  static const PosCategory pets = PosCategory._(14, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'pets');
  static const PosCategory education = PosCategory._(9, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'education');
  static const PosCategory furniture = PosCategory._(11, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'furniture');
  static const PosCategory fuel = PosCategory._(12, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'fuel');
  static const PosCategory pharmacy = PosCategory._(13, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'pharmacy');

  static const $core.List<PosCategory> values = <PosCategory> [
    other,
    fitness,
    food,
    beauty,
    clothes,
    hardware,
    grocery,
    medicine,
    entertainment,
    travel,
    pets,
    education,
    furniture,
    fuel,
    pharmacy,
  ];

  static final $core.Map<$core.int, PosCategory> _byValue = $pb.ProtobufEnum.initByValue(values);
  static PosCategory? valueOf($core.int value) => _byValue[value];

  const PosCategory._($core.int v, $core.String n) : super(v, n);
}

class Sex extends $pb.ProtobufEnum {
  static const Sex undefined = Sex._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'undefined');
  static const Sex male = Sex._(1, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'male');
  static const Sex female = Sex._(2, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'female');

  static const $core.List<Sex> values = <Sex> [
    undefined,
    male,
    female,
  ];

  static final $core.Map<$core.int, Sex> _byValue = $pb.ProtobufEnum.initByValue(values);
  static Sex? valueOf($core.int value) => _byValue[value];

  const Sex._($core.int v, $core.String n) : super(v, n);
}

class Level extends $pb.ProtobufEnum {
  static const Level basic = Level._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'basic');

  static const $core.List<Level> values = <Level> [
    basic,
  ];

  static final $core.Map<$core.int, Level> _byValue = $pb.ProtobufEnum.initByValue(values);
  static Level? valueOf($core.int value) => _byValue[value];

  const Level._($core.int v, $core.String n) : super(v, n);
}

class Property_Known extends $pb.ProtobufEnum {
  static const Property_Known age = Property_Known._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'age');
  static const Property_Known skllzz = Property_Known._(1, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'skllzz');

  static const $core.List<Property_Known> values = <Property_Known> [
    age,
    skllzz,
  ];

  static final $core.Map<$core.int, Property_Known> _byValue = $pb.ProtobufEnum.initByValue(values);
  static Property_Known? valueOf($core.int value) => _byValue[value];

  const Property_Known._($core.int v, $core.String n) : super(v, n);
}


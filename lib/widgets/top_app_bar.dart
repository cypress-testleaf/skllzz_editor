import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../skllzz_app_icons.dart';
import '../theming.dart';

class TopAppBar extends StatelessWidget implements PreferredSizeWidget {
  /// label используется также для создания всплывающего пояснения
  final String label;
  final Widget? child;
  final bool hasBlur;
  final bool hasBackArrow;
  final double? toolbarHeight;
  final PreferredSizeWidget? bottom;
  @override
  final Size preferredSize;

  TopAppBar(
      {Key? key,
      this.label = "SKLLZZ",
      this.hasBlur = false,
      this.hasBackArrow = false,
      this.toolbarHeight,
      this.bottom,
      this.child})
      : preferredSize = Size.fromHeight(toolbarHeight ??
            kToolbarHeight + (bottom?.preferredSize.height ?? 0.0));

  @override
  Widget build(BuildContext context) {
    if (hasBlur) {
      return AppBar(
        brightness: Brightness.dark,
        backgroundColor: Colors.transparent,
        automaticallyImplyLeading: false,
        elevation: 0,
        centerTitle: true,
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: [Styles.backgroundColor, Colors.transparent],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                stops: [0.4, 0.5],
                tileMode: TileMode.clamp),
          ),
        ),
        textTheme: TextTheme(
            headline6:
            Theme.of(context).appBarTheme.textTheme!.headline6!.copyWith(
                      color: Styles.actionColor,
                    )),
        title: Tooltip(
          message: label,
          child: child ?? Text(label, textAlign: TextAlign.center),
        ),
        leading: BackdropFilter(
            filter: ImageFilter.blur(sigmaX: 5.0, sigmaY: 5.0),
            child: (hasBackArrow)
                ? IconButton(
                    splashRadius: 16,
                    splashColor: Styles.actionColor.withOpacity(0.12),
                    highlightColor: Styles.actionColor.withOpacity(0.1),
                    icon: Icon(SkllzzAppIcons.back, color: Styles.actionColor),
                    onPressed: () => Navigator.pop(context),
                  )
                : Container()),
        bottom: bottom,
      );
    } else {
      return AppBar(
        brightness: Brightness.dark,
        backgroundColor: Colors.transparent,
        automaticallyImplyLeading: false,
        elevation: 0,
        centerTitle: true,
        textTheme: TextTheme(
            headline6:
            Theme.of(context).appBarTheme.textTheme!.headline6!.copyWith(
                      color: Styles.actionColor,
                    )),
        title: Tooltip(
          message: label,
          child: child ?? SelectableText(label, textAlign: TextAlign.center),
        ),
        leading: (hasBackArrow)
            ? IconButton(
                splashRadius: 16,
                splashColor: Styles.actionColor.withOpacity(0.12),
                highlightColor: Styles.actionColor.withOpacity(0.1),
                icon: Icon(SkllzzAppIcons.back, color: Styles.actionColor),
                onPressed: () => Navigator.pop(context),
              )
            : null,
      );
    }
  }
}

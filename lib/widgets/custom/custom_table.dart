import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import '../../theming.dart';

typedef WrapperBuilder = Widget Function(BuildContext, Widget);

/// Пример вызова:
/// BuildColumns(children: headerContent, wrappers: widthWrappers).build(context)
class BuildColumns {
  final List<Widget>? children;
  final List<WrapperBuilder>? wrappers;

  BuildColumns({required this.children, required this.wrappers});

  List<Widget> build(BuildContext context) {
    final List<Widget> columns = List<Widget>.empty(growable: true);
    for (int i = 0; i < children!.length; i++) {
      columns.add(wrappers![i](context, children![i]));
    }
    return columns;
  }
}

class TableBuilder<T> extends StatelessWidget {
  /// список выводимых в таблицу объектов
  final List<T>? list;

  /// содержимое заголовочной строки
  /// настройки ширины включать не нужно
  /// Например:
  /// headerContent: [
  ///               Text(""),
  ///               Text("Имя"),
  ///               Text("Права"),
  ///               Text("Действия")
  ///             ],
  final List<Widget>? headerContent;

  /// обертка над строкой заголовка
  final Function(BuildContext, List<Widget>?)? headerWrapper;

  /// обертка над строкой, выводящей объект
  /// полностью меняет внешний вид строки
  final Widget Function(BuildContext, List<Widget>)? rowWrapper;

  /// содержимое строки, выводящей объект
  /// настройки ширины включать не нужно
  /// Например:
  /// rowContent: [
  ///               Icon(Icons.play),
  ///               SelectableText("${item.id}"),
  ///             ],
  final List<Widget> Function(BuildContext, T)? rowContent;

  /// регулирование ширины колонок
  /// Например:
  /// widthWrappers: [
  ///     (context, child) => Container(
  ///           alignment: Alignment.center,
  ///           width: 48,
  ///           child: child,
  ///         ),
  ///     (context, child) => Expanded(
  ///         flex: 1,
  ///         child: Container(
  ///             alignment: Alignment.centerLeft,
  ///             padding: EdgeInsets.symmetric(horizontal: 8),
  ///             child: child)),
  ///     (context, child) => Expanded(
  ///           flex: 2,
  ///           child: Container(
  ///               alignment: Alignment.centerLeft,
  ///               padding: EdgeInsets.symmetric(horizontal: 8),
  ///               child: child),
  ///         ),
  ///     (context, child) => Container(
  ///         width: 10 * 18 * MediaQuery.of(context).textScaleFactor,
  ///         alignment: Alignment.center,
  ///         padding: EdgeInsets.symmetric(horizontal: 8),
  ///         child: child)
  ///   ],
  final List<WrapperBuilder>? widthWrappers;

  final ScrollController? scrollController;

  final InkWell Function(T)? inkWellGestures;

  TableBuilder(
      {Key? key,
        this.list,
        this.headerContent,
        this.headerWrapper,
        this.rowWrapper,
        this.rowContent,
        this.widthWrappers,
        this.scrollController,
        this.inkWellGestures})
      : super(key: key);

  final ScrollController _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    List<Widget>? header;
    if (widthWrappers != null && widthWrappers!.isNotEmpty) {
      header = BuildColumns(children: headerContent, wrappers: widthWrappers)
          .build(context);
    } else {
      header = headerContent;
    }
    return Column(children: [
      if (headerContent != null)
        if (headerWrapper != null)
          headerWrapper!(context, header)
        else
          Card(
              color: Styles.brandColor,
              elevation: 0,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(21.0))),
              child: Container(
                  constraints: BoxConstraints(minHeight: 40),
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: DefaultTextStyle(
                      style: Theme.of(context)
                          .textTheme
                          .subtitle1!
                          .copyWith(color: Styles.backgroundColor),
                      child: Row(children: header!)))),
      if (list != null && list!.isNotEmpty)
        Expanded(
          child: CupertinoScrollbar(
            controller: _scrollController,
            isAlwaysShown: false,
            thickness: 5,
            thicknessWhileDragging: 10,
            radius: Radius.circular(5),
            radiusWhileDragging: Radius.circular(10),
            child: Padding(
                padding: const EdgeInsets.only(bottom: 40.0),
                child: Stack(clipBehavior: Clip.none, children: [
                  ListView.builder(
                    controller: _scrollController,
                    padding: const EdgeInsets.only(
                        top: 10.0, bottom: 40.0, right: 15),
                    itemCount: list!.length,
                    shrinkWrap: true,
                    itemBuilder: (BuildContext context, int index, {Key? key}) {
                      final T item = list![index];
                      if (item == null || rowContent == null) {
                        return Container();
                      }
                      List<Widget> row;
                      if (widthWrappers != null && widthWrappers!.isNotEmpty) {
                        row = BuildColumns(
                            children: rowContent!(context, item),
                            wrappers: widthWrappers)
                            .build(context);
                      } else {
                        row = rowContent!(context, item);
                      }
                      if (rowWrapper != null) {
                        return rowWrapper!(context, row);
                      } else {
                        return Padding(
                          padding: const EdgeInsets.symmetric(vertical: 4.0),
                          child: Material(
                              color: Styles.backgroundCard,
                              elevation: 0,
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(21.0))),
                              child: (inkWellGestures != null)
                                  ? InkWell(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(21.0)),
                                onTap: inkWellGestures!(item).onTap,
                                onDoubleTap:
                                inkWellGestures!(item).onDoubleTap,
                                onTapDown:
                                inkWellGestures!(item).onTapDown,
                                onHover: inkWellGestures!(item).onHover,
                                onLongPress:
                                inkWellGestures!(item).onLongPress,
                                child: _buildItem(context, row),
                              )
                                  : _buildItem(context, row)),
                        );
                      }
                    },
                  ),
                  Positioned(
                    top: -5,
                    left: 0,
                    right: 0,
                    child: Container(
                      height: 20,
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                            colors: [
                              Styles.backgroundColor,
                              Styles.backgroundColor.withOpacity(0)
                            ],
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            tileMode: TileMode.clamp),
                      ),
                    ),
                  ),
                  Positioned(
                      bottom: -5,
                      left: 0,
                      right: 0,
                      child: Container(
                        height: 40,
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                              colors: [
                                Styles.backgroundColor,
                                Styles.backgroundColor.withOpacity(0)
                              ],
                              begin: Alignment.bottomCenter,
                              end: Alignment.topCenter,
                              tileMode: TileMode.clamp),
                        ),
                      ))
                ])),
          ),
        ),
    ]);
  }

  Widget _buildItem(BuildContext context, List<Widget> children) {
    return Container(
        constraints: BoxConstraints(minHeight: 40),
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: DefaultTextStyle(
            style: Theme.of(context).textTheme.bodyText2!,
            child: Row(
              children: children,
            )));
  }
}

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:skllzz_editor/providers/info_provider.dart';

import '../../theming.dart';
import 'error.dart';

Future<dynamic> showInfoDialog(BuildContext context, String info,
    [Widget? additionalInfo]) {
  final children = <Widget>[
    Text(
      "Информация\n",
      textAlign: TextAlign.center,
      style: Theme.of(context).textTheme.bodyText2,
    ),
  ];

  if ((info).isNotEmpty) {
    children.add(Container(height: 15));
    children.add(Text(info,
        style: Theme.of(context).textTheme.subtitle1,
        textAlign: TextAlign.center));
  }

  if (additionalInfo != null) {
    children.add(DefaultTextStyle(
        child: additionalInfo, style: Theme.of(context).textTheme.bodyText2!));
  }

  return showDialog(
      context: context,
      barrierDismissible: true,
      builder: (context) => ProviderScope(child: AlertDialog(
              backgroundColor: Styles.backgroundInfo,
              title: Text('Информация', textAlign: TextAlign.center),
              content: SingleChildScrollView(
                child: ListBody(children: children),
              ),
              actions: <Widget>[
                Center(
                  child: TextButton(
                    child: Text(
                      'ОК',
                      style: Theme.of(context)
                          .textTheme
                          .button!
                          .copyWith(color: Styles.backgroundColor),
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ),
              ])));
}

class ShowInfo extends StatefulWidget {
  final Widget child;

  const ShowInfo({Key? key, required this.child}) : super(key: key);

  @override
  _ShowInfoState createState() => _ShowInfoState();
}

class _ShowInfoState extends State<ShowInfo> {
  StreamSubscription? sub;

  @override
  void initState() {
    super.initState();
    sub = context.read(infoProvider).stream.listen((info) {
      if (info != null) {
        Widget? helpWidget;

        String msg = info.toString();

        if (info is InfoHelp) {
          helpWidget = info.help;
        }

        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          backgroundColor: Styles.backgroundInfo,
          content: InkWell(
              onTap: () {
                ScaffoldMessenger.of(context).hideCurrentSnackBar();
              },
              child: Text(
                msg,
                style: Theme.of(context)
                    .textTheme
                    .caption!
                    .copyWith(color: Styles.backgroundColor),
              )),
          action: SnackBarAction(
            onPressed: () {
              ScaffoldMessenger.of(context).hideCurrentSnackBar();
              if (helpWidget == null) {
                showInfoDialog(context, info as String);
              } else {
                showInfoDialog(context, info as String, helpWidget);
              }
            },
            label: "Подробнее...",
            textColor: Styles.backgroundColor,
          ),
        ));
      }
    });
  }

  @override
  void dispose() {
    sub?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return widget.child;
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:skllzz_editor/generated/grpc/skllzz/management/space.pb.dart';
import 'package:skllzz_editor/providers/info_provider.dart';
import 'package:skllzz_editor/providers/provider.dart';
import 'package:skllzz_editor/providers/space_auth_provider.dart';
import 'package:skllzz_editor/widgets/adaptive/adaptive_widget.dart';
import 'package:skllzz_editor/widgets/custom/custom_list_tile.dart';

import '../../skllzz_app_icons.dart';
import '../../theming.dart';
import 'error.dart';

class ChooseSpaceScreen extends ConsumerWidget {
  ChooseSpaceScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    final spacesList = watch(spaceListProvider);
    return spacesList.when(
        data: (spaces) {
          if ((spaces).isEmpty) {
            return Center(
              child: Column(mainAxisSize: MainAxisSize.min, children: [
                Text(
                  "У Вас отсуствует доступ к чему либо!",
                  style: Theme.of(context).textTheme.subtitle2,
                ),
                Container(
                    width: 100, height: 100, child: Styles.loadingAnimation),
                Text(
                  "Если у вас есть код билета,\n"
                  "нажмите на кнопку \"Войти в пространство\"",
                  style: Theme.of(context).textTheme.caption,
                  textAlign: TextAlign.center,
                ),
                Container(height: 20),
                EnterSpaceButton()
              ]),
            );
          }
          return Center(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  ListView.custom(
                    scrollDirection: Axis.vertical,
                    padding: const EdgeInsets.only(top: 4.0),
                    shrinkWrap: true,
                    childrenDelegate: SliverChildBuilderDelegate(
                      (BuildContext context, int index) {
                        if (index == 0) {
                          return Column(children: [
                            Container(
                              alignment: Alignment.centerLeft,
                              padding: EdgeInsets.only(left: 15, bottom: 15),
                              child: Text(
                                "Доступные пространства",
                                style: Theme.of(context)
                                    .textTheme
                                    .caption!
                                    .copyWith(color: Colors.white),
                              ),
                            ),
                            Container(child: _buildItem(context, spaces[index]))
                          ]);
                        }
                        return Container(
                            child: _buildItem(context, spaces[index]));
                      },
                      childCount: spaces.length,
                      // findChildIndexCallback: (Key key) {
                      //   final ValueKey valueKey = key;
                      //   final String data = valueKey.value;
                      //   return LoadSpaces.instance.spacesPos[data];
                      // }
                    ),
                  ),
                  Container(height: 20),
                  EnterSpaceButton()
                ]),
          );
        },
        loading: () => Center(
              child: Column(mainAxisSize: MainAxisSize.min, children: [
                Text(
                  "У Вас отсуствует доступ к чему либо!",
                  style: Theme.of(context).textTheme.subtitle2,
                ),
                Container(
                    width: 100, height: 100, child: Styles.loadingAnimation),
                Text(
                  "Если у вас есть код билета,\n"
                  "нажмите на кнопку \"Войти в пространство\"",
                  style: Theme.of(context).textTheme.caption,
                  textAlign: TextAlign.center,
                ),
                Container(height: 20),
                EnterSpaceButton()
              ]),
            ),
        error: (e, s) {
          context.read(errorProvider).state =
              ExceptionCause(e, "Ошибка загрузки пространств");
          return LoadingErrorMessage(
              message: "Не удалось загрузить пространства");
        });
  }

  Widget _buildItem(BuildContext context, Space item) {
    return Consumer(builder: (context, watch, _) {
      return CustomListTile(
          onTap: () {
            context.read(currentSpaceProvider).space = item;
          },
          leading: Radio<Space>(
              value: item,
              groupValue: watch(currentSpaceProvider.state),
              activeColor: Styles.brandColor,
              onChanged: (value) {
                context.read(currentSpaceProvider).space = value!;
              }),
          label: Text(
            item.name,
            style: Theme.of(context).textTheme.subtitle1,
            textAlign: TextAlign.left,
            softWrap: true,
          ),
          divider: Divider(
            color: Styles.backgroundCard,
            thickness: 2,
          ));
    });
  }
}

class EnterSpaceButton extends ConsumerWidget {
  final TextEditingController _ticketController = TextEditingController();

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    return Container(
      child: ElevatedButton.icon(
          icon: Icon(SkllzzAppIcons.key),
          label: Text("Войти в пространство"),
          onPressed: () async {
            try {
              await Clipboard.getData('text-plain').then((value) {
                _ticketController.text = value!.text!;
                return value;
              });
            } catch (e) {
              context.read(errorProvider).state = ExceptionCause(
                  e, "Пользователь запретил использовать буфер обмена. $e");
            }
            final newSpace = await showDialog(
                context: context,
                builder: (context) {
                  return ProviderScope(
                      child: AlertDialog(
                    title: Text(
                      "Введите код билета",
                      style: Theme.of(context).textTheme.caption,
                    ),
                    content: AdaptiveWidget(
                      tightWidth: 600 + Styles.currentNavWidth,
                      tightChild: TextField(
                        controller: _ticketController,
                        autofocus: true,
                        minLines: 1,
                        maxLines: 10,
                        decoration: InputDecoration(
                            labelText: "Код билета",
                            hintText: "Введите код билета"),
                      ),
                      wideChild: Container(
                        width: (MediaQuery.of(context).size.width -
                                Styles.currentNavWidth) *
                            0.4,
                        child: TextField(
                          controller: _ticketController,
                          autofocus: true,
                          minLines: 1,
                          maxLines: 10,
                          decoration: InputDecoration(
                              labelText: "Код билета",
                              hintText: "Введите код билета"),
                        ),
                      ),
                    ),
                    actions: [
                      ElevatedButton(
                          onPressed: () async {
                            try {
                              InviteTicket ticket = await context
                                  .read(spaceAccessClientProvider)
                                  .join(InviteTicket()
                                    ..id = _ticketController.text)
                                  .onError((e, s) {
                                context.read(errorProvider).state =
                                    ExceptionCause(
                                        e, "Ошибка входа по приглашению");
                                return InviteTicket();
                              });
                              if (ticket.id.isNotEmpty) {
                                Navigator.of(context)
                                    .pop(Space()..id = ticket.spaceId);
                              }
                            } catch (e) {
                              showErrorDialog(context, e);
                            }
                          },
                          child: Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 15.0),
                            child: Text("Войти"),
                          )),
                      TextButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          child: Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 15.0),
                            child: Text("Отмена"),
                          ))
                    ],
                  ));
                });
            if (newSpace != null) {
              context.read(currentSpaceProvider).space = newSpace;
            }
          }),
    );
  }
}
